variable "prefix"{
    default = "genaro"
    type = string
}

variable "location"{
    default = "East US"
    type = string
}